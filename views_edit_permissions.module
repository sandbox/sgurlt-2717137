<?php

/**
 * @file
 * Module file of the views edit permissions module.
 */

/**
 * Implements hook_permission().
 */
function views_edit_permissions_permission() {
  $views = views_get_all_views();
  $permissions = array();

  foreach ($views as $view) {
    $permissions['administer view ' . $view->name] = array(
      'title' => t('Administer View: !view_name', array('!view_name' => $view->name)),
    );
  }

  return $permissions;
}

/**
 * Implements hook_menu_alter().
 */
function views_edit_permissions_menu_alter(&$items) {
  $access = array(
    'access callback' => 'views_edit_permissions_access_check',
    'access arguments' => array(4),
  );

  $menu_items = array(
    'admin/structure/views/view/%views_ui_cache',
    'admin/structure/views/view/%views_ui_cache/edit',
    'admin/structure/views/view/%views_ui_cache/edit/%/ajax',
    'admin/structure/views/view/%views_ui_cache/preview/%',
    'admin/structure/views/view/%views_ui_cache/preview/%/ajax',
    'admin/structure/views/view/%views_ui_cache/break-lock',
  );

  $items['admin/structure/views/view/%views_ui_cache'] = array_replace($items['admin/structure/views/view/%views_ui_cache'], $access);

  foreach ($menu_items as $menu_item) {
    $items[$menu_item] = array_replace($items[$menu_item], $access);
  }

  $items['admin/structure/views/nojs/%/%views_ui_cache']['access callback'] = 'views_edit_permissions_access_check';
  $items['admin/structure/views/nojs/%/%views_ui_cache']['access arguments'] = array(5);

  $items['admin/structure/views/ajax/%/%views_ui_cache']['access callback'] = 'views_edit_permissions_access_check';
  $items['admin/structure/views/ajax/%/%views_ui_cache']['access arguments'] = array(5);

  $ajax_callbacks = array(
    'preview' => 'views_ui_preview',
  );
  foreach ($ajax_callbacks as $menu => $menu_callback) {
    $items['admin/structure/views/nojs/' . $menu . '/%views_ui_cache/%']['access callback'] = 'views_edit_permissions_access_check';
    $items['admin/structure/views/nojs/' . $menu . '/%views_ui_cache/%']['access arguments'] = array(5);

    $items['admin/structure/views/ajax/' . $menu . '/%views_ui_cache/%']['access callback'] = 'views_edit_permissions_access_check';
    $items['admin/structure/views/ajax/' . $menu . '/%views_ui_cache/%']['access arguments'] = array(5);
  }
}

/**
 * Set new views permissions.
 */
function views_edit_permissions_access_check($view) {
  if (user_access('administer view ' . $view->name)) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}
